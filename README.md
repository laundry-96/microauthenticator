# microAuthenticator
Little demo microservice for Accenture's SRE Week

## Run
`./gradlew bootRun`

## CURL requests for hitting API endpoints
curl -X POST -d "username=test&password=password&fullName=Test Name" localhost:8080/authenticate/register
curl -X PUT -d "username=test&password=password&newFullName=New Test Name" localhost:8080/authenticate/updateUserData
curl -G -d "username=test&password=password" localhost:8080/authenticate/getUserData
curl -X DELETE -d "username=test&password=password" localhost:8080/authenticate/unregister
