# Point to where we should store the TF State
terraform {
  backend "s3" {
    bucket = "tfstatebucket-microauth"
    key = "terraform.tfstate"
    region = "us-east-1"
  }
}

# Access key and secret key are loaded from the environment (AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY)
provider "aws" {
  region = "us-east-1"
}



variable "SSH_PUB" {
  type = string
  description = "Public key to be placed on the ec2 instance"
}

resource "aws_key_pair" "lab-key" {
  key_name = "auth-gitlab-deploy-key"
  public_key = var.SSH_PUB
}

# Create a Security Group to allow SSH
resource "aws_security_group" "micro-auth-ssh" {
  name = "micro-auth-ssh"

  # Incoming traffic
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allow from anywhere
  }

  # Outgoing trafic
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Create the ec2 instance, and install OpenJDK 11 
resource "aws_instance" "micro-auth" {

  ami = "ami-04505e74c0741db8d"
  instance_type = "t2.micro"
  vpc_security_group_ids = [ aws_security_group.micro-auth-ssh.id ]

  tags = {
    name = "authenticator-1"
  }
  
  key_name=aws_key_pair.lab-key.key_name

  connection {
    type = "ssh"
    host = self.public_ip
    user = "ubuntu"
    private_key = file("lab.pub")
    timeout = "4m"
  }

  user_data = <<-EOF
    #!/bin/bash 
    apt update && apt install -y openjdk-11-jdk
    EOF
  
}


output "instance_ip" {
  value = aws_instance.micro-auth.public_ip
}
