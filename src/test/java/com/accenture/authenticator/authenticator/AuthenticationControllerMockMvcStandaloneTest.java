package com.accenture.authenticator.authenticator;

import com.accenture.authenticator.authenticator.account.Account;
import com.accenture.authenticator.authenticator.account.AccountRepository;
import com.accenture.authenticator.authenticator.account.AuthorizeController;
import com.accenture.authenticator.authenticator.account.ExceptionResponseHandler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@ExtendWith(MockitoExtension.class)
public class AuthenticationControllerMockMvcStandaloneTest {

    private MockMvc mvc;

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AuthorizeController authenticationController;

    // This will be initialized via the initFields method below
    private JacksonTester<Account> jsonAccount;

    // The BeforeEach annotation means, run this before each test
    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());

        mvc = MockMvcBuilders.standaloneSetup(authenticationController)
                .setControllerAdvice(new ExceptionResponseHandler())
                .build();
    }

    @Test
    public void canRegisterAccount() throws Exception {
        MockHttpServletResponse response = mvc.perform(
            post("/authenticate/register")
                .param("username", "laundry-96")
                .param("password", "password")
                .param("fullName", "Austin DeLauney")
            ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.CREATED.value());
        assertThat(response.getContentAsString()).isEqualTo("Successfully registered account laundry-96.");
    }

    @Test
    public void canGetUserData() throws Exception {

        given(accountRepository.getById("laundry-96"))
                .willReturn(new Account("laundry-96", authenticationController.hashpw("password"), "Austin DeLauney"));

        MockHttpServletResponse response = mvc.perform(
            get("/authenticate/getUserData")
                .param("username", "laundry-96")
                .param("password", "password")
            ).andReturn().getResponse();

        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("Austin DeLauney");
    }
}
