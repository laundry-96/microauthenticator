package com.accenture.authenticator.authenticator;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AuthenticatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthenticatorApplication.class, args);
	}

	@Bean
	public OpenAPI customOpenAPI(@Value("${application-name}") String appName, @Value("${application-description}") String appDescription, @Value("$application-version}") String appVersion) {
		return new OpenAPI()
				.info(new Info()
						.title(appName)
						.version(appVersion)
						.description(appDescription)
						.license(new License()
								.name("MIT")
								.url("https://mit-license.org/")));
	}
}
