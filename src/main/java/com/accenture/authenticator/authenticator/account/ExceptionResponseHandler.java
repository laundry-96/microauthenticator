package com.accenture.authenticator.authenticator.account;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
@RestController
public class ExceptionResponseHandler {

    // Handle the AccountAuthenticationErrorException
    // Returns FORBIDDEN because we don't want to say if the password was wrong, so generic response is fine
    // Why do we not want the user to know if the password was wrong? It means the user is correct, so they can
    // keep brute forcing a password
    @ExceptionHandler(AccountAuthenticationErrorException.class)
    public final ResponseEntity<GenericExceptionResponse> handleAccountAuthenticationErrorException(AccountAuthenticationErrorException ex) {
        GenericExceptionResponse gER = new GenericExceptionResponse(new Date(), ex.getMessage());
        System.out.println(ex.getMessage());
        return new ResponseEntity<>(gER, HttpStatus.FORBIDDEN);
    }

    // Handle all of our exceptions we don't specify in a generic way
    // This is good for our use case, since our exceptions can handle the details the user needs to know
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<GenericExceptionResponse> handleAllExceptions(Exception ex) {
        // Create a new GenericExceptionResponse. Use new Date(), since creating a new instance of Date uses the current timestamp
        GenericExceptionResponse gER = new GenericExceptionResponse(new Date(), ex.getMessage());
        System.out.println(ex.getMessage());
        return new ResponseEntity<>(gER, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
