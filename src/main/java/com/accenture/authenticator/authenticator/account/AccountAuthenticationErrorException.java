package com.accenture.authenticator.authenticator.account;

public class AccountAuthenticationErrorException extends RuntimeException {
    public AccountAuthenticationErrorException(String username) {
        super ("Could not authenticate account " + username + ". Please make sure the username and password are correct");
    }
}
