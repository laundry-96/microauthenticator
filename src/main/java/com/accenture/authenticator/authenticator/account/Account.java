package com.accenture.authenticator.authenticator.account;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Account {

    @Id
    private String username;
    private String hashedPassword;
    private String fullName;

    public Account() {}

    public Account(String username, String hashedPassword, String fullName) {
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.fullName = fullName;
    }

    @Override
    public String toString() {
        return "Account{" +
                "username=\'" + username + "\'" +
                ", hashedPassword=\'" + hashedPassword + "\'" +
                ", fullName=\'" + fullName + "\'" +
                "}";
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getHashedPassword() { return this.hashedPassword; }

    public void setHashedPassword(String hashedPassword) { this.hashedPassword = hashedPassword; }

    public String getFullName() { return fullName; }

    public void setFullName(String fullName) { this.fullName = fullName; }
}
