package com.accenture.authenticator.authenticator.account;

import org.springframework.http.HttpStatus;

import java.util.Date;

public class GenericExceptionResponse {
    private Date timestamp;
    private String message;

    public GenericExceptionResponse(Date timestamp, String message) {
        this.timestamp = timestamp;
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }
}
