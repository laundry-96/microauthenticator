package com.accenture.authenticator.authenticator.account;

import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import org.mindrot.jbcrypt.BCrypt;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;

@RestController
@RequestMapping("/authenticate")
public class AuthorizeController {
    AccountRepository accountRepository;
    String salt;

    public AuthorizeController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
        salt = BCrypt.gensalt(10);
    }

    public String hashpw(String password) {
        return BCrypt.hashpw(password, salt);
    }

    private Account authenticate(String username, String password) {
        Account toAuth;
        try {
            toAuth = accountRepository.getById(username);
        } catch (EntityNotFoundException e) {
            throw new AccountAuthenticationErrorException(username);
        }

        String hashedPW = hashpw(password);
        if (!hashedPW.equals(toAuth.getHashedPassword())) {
            throw new AccountAuthenticationErrorException(username);
        }

        return toAuth;
    }

    @PostMapping("/register")
    ResponseEntity postAccount(@RequestParam String username, @RequestParam String password, @RequestParam String fullName) {

        if(accountRepository.existsById(username)) {
            System.out.println("Someone tried to register a username that already exists: " + username);
            return new ResponseEntity("An error occured", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String hashedPassword = hashpw(password);

        Account newAccount = new Account();
        newAccount.setUsername(username);
        newAccount.setHashedPassword(hashedPassword);
        newAccount.setFullName(fullName);

        System.out.println("A new account has been created: " + newAccount);

        accountRepository.save(newAccount);

        return new ResponseEntity("Successfully registered account " + username + ".", HttpStatus.CREATED);
    }

    @GetMapping("/getUserData")
    ResponseEntity getUserData(@RequestParam String username, @RequestParam String password) {
        Account requestedAccount = authenticate(username, password);
        System.out.println("The user " + username + " has retrieved their data");
        return new ResponseEntity(requestedAccount.getFullName(), HttpStatus.OK);
    }

    @PutMapping("/updateUserData")
    ResponseEntity updateUserData(@RequestParam String username, @RequestParam String password, @RequestParam String newFullName) {
        Account toUpdate = authenticate(username, password);
        toUpdate.setFullName(newFullName);
        System.out.println("The user " + username + " has set their name to " + newFullName);
        accountRepository.save(toUpdate);
        return new ResponseEntity("Full name is now " + newFullName + ".", HttpStatus.OK);
    }

    @DeleteMapping("/unregister")
    ResponseEntity deleteAccount(@RequestParam String username, @RequestParam String password) {
        Account toDelete = authenticate(username, password);
        accountRepository.delete(toDelete);
        System.out.println("The user " + username + " has deleted their account");
        return new ResponseEntity("Account " + username + " deleted successfully.", HttpStatus.OK);
    }
}
